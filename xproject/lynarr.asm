%include "asm_io.inc" 

section .data
	ERROR_MSG1: dd "Incorrect Number of Arguments. Lynarr Takes One Argument,  Ex. lynarr XXX", 0
	ERROR_MSG2: dd "Input String Must Contain Between 1 to 20 Characters.", 0
	ERROR_MSG3: dd "Input String Can Only Contain Lower Case Letters. Ex.  a,b,c...", 0

section .bss
	N: resd 1                ;Contains Length of Input String
	F: resd 1 		 ;Flag 
	K: resd 1
	X: resb 20               ;stores chracters of input string 
	Y: resd 20 		 ;stores integers of MaxLyn subroutine 

section .text 
	global asm_main
;;;;;;;;;;;;;;;;;;;;;;;;; subroutine DISPLAY
;Takes Arguments Z,n,flag 
display: 
	enter 0,0
	pusha 

	mov ebx, [ebp+8]   		;Flag    0-string 1-int
	mov ecx, [ebp+12] 		;length
	mov edx, [ebp+16] 		;array  

	cmp ebx, dword 0 
	   jne DISPLAY_INT

	DISPLAY_STR:cmp ebx, ecx 
	je END_DISPLAY 
 	   mov eax, [edx]
	   call print_char 
	   inc ebx 
	   inc edx 
	jmp DISPLAY_STR 
	 
	DISPLAY_INT:cmp ebx, ecx 
	jg END_DISPLAY
	   mov eax, [edx] 
	   call print_int 
	   mov eax, ' '
	   call print_char 
	   inc ebx 
	   add edx, 4 
	jmp DISPLAY_INT 

	END_DISPLAY: 
	   call read_char 	   	  ;wait for user input 
	   popa
	   leave 
	   ret 

;;;;;;;;;;;;;;;;;;;;;;;;;;  subroutine MAXLYN 
; takes arguments Z,n,k
maxLyn: 
	enter 0,0 
	pusha 

	mov edx, [ebp+16] 		  ; Array X
	mov ecx, [ebp+12]		  ; Length N 
	mov ebx, [ebp+8] 		  ; Index K
	mov edi, dword 1 		  ; Value p 
	mov esi, ebx 
	inc esi 			  ; index i = k+1 

	mov eax, ecx
	dec eax				  ;n-1
	cmp ebx, eax
	   je RETURN			  ; if k = n-1 return 1 

	mov eax, 0 
	INDEX_LOOP:cmp esi, dword[N]   	  ; while i < N 
	je RETURN 
	   mov ecx, esi
	   sub ecx, edi 
	   mov al, byte[edx+ecx]	  ;x[i-p]
	   cmp al, byte[edx+esi] 
	   jne LOOP
	   je NEXT
		LOOP:cmp al, byte[edx+esi] 
		jg RETURN 
		   mov edi, esi 
		   add edi, 1
		   sub edi, ebx           ; p = i + 1 - k 
	   NEXT: 
	   inc esi 
	jmp INDEX_LOOP

	RETURN: 
	  mov eax, ebx 
	  mov ebx, dword 4 
	  mul ebx
	  mov [Y+eax], dword edi 
	popa
	leave 
	ret 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; MAIN 
asm_main: 
	enter 0,0 
	pusha 

	mov eax, dword [ebp+8]    		;Get argc, number of arguments
	cmp eax, dword 2 
	   jne ARG_ERROR          		;if more than two arguments send error and terminate 

	mov ebx, dword[ebp+12]    		;address of argv
	mov ecx, dword[ebx+4]     		;Get Second Command Line Argument, input string
        mov al, byte[ecx] 	  		;first charcter of string
	mov edx, 0    

	CHECK_LOOP: cmp al, 0 
	je END_CHECK
	   mov [X+edx], al   		        ;Store Character in Array Element
	   add edx, 1            		;increment counter 
	   cmp edx, dword 20        		;check to see if input string exceeds chracter limit 
	      jg LEN_ERROR 

	   cmp al, 'A' 
	     jb NOT_UC
	   cmp al, 'Z' 
	     ja NOT_UC
	     jmp CHR_ERROR      		;Character was uppercase  

	   NOT_UC: 
	      add ecx, 1
	      mov al, byte[ecx] 
	jmp CHECK_LOOP
	END_CHECK:
	mov [N], dword edx

	;Display Input String 
	mov [F], dword 0     			;Flag Set to display array of chracters 
	push X
	push dword[N] 
	push dword[F]  
	call display
	add esp, 12 
		
	;MaxLyn Routine 
	mov [K], dword 0          ;index k 
	mov eax, dword [K] 
	MAXLYN:cmp eax, dword[N] 		;k = 0..n-1
	je END_MAXLYN
	   push X 
	   push dword[N] 
	   push dword[K] 
	   call maxLyn 
	   add esp, 12 
	   mov eax, [K]
	   add eax, dword 1	       			;Next Index  k+1 
	   mov [K], dword eax
	jmp MAXLYN 
	END_MAXLYN: 

	;Display Integers Returned by MaxLyn 
	mov [F],dword 1 			; Flag Set to display array of integers 
	push Y
	push dword[N]
	push dword[F] 
	call display 
	add esp, 12 

	jmp END 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ERROR MESSAGES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;       
	ARG_ERROR: 
           mov eax, ERROR_MSG1
	   call print_string
	   call print_nl
	   jmp END

	LEN_ERROR: 
	   mov eax, ERROR_MSG2
	   call print_string
	   call print_nl
	   jmp END 
	
	CHR_ERROR: 
	   mov eax, ERROR_MSG3
	   call print_string
	   call print_nl
	   jmp END 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
	END: 	
       	  popa 
	  leave 
	  ret 
