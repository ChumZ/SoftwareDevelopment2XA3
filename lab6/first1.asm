%include "asm_io.inc" 

section .data
str: db "123456",0 
chr: db 'A'
int: dd 1,2 

section .txt 
	global asm_main 
asm_main: 
	enter 0,0
	pusha

 	mov eax, str ;Display String On Line 
	call print_string
	call print_nl 

	mov eax, [int] ;Print Out Integers
	call print_int 
	mov eax, ' '
	call print_char 
	mov eax, [int+4]
	call print_int 
	call print_nl 

	mov al, byte[chr]  ;Print Out Character
	call print_char
	call print_nl 

	mov [str], byte 'A'
	mov [str+1], byte 'B'
	mov [str+2], byte 'C'
	mov [str+3], byte 0

	mov eax, str
	call print_string
	call print_nl 

	mov [int], dword 7 
	mov [int+4], dword 8

	mov eax, [int] 
	call print_int
	mov eax, ' '
	call print_char
	mov eax, [int+4]
	call print_int
	call print_nl 

	mov [chr], byte 'B' 

	mov al, byte[chr]
	call print_char
	call print_nl 
	
	popa
	mov eax,0
	leave
	ret 
