%include "asm_io.inc" 
global asm_main

section .data

section .bss

section .text 
	
asm_main: 
	enter 0,0 
	pusha
	
	mov ebx, dword[ebp+12] 
	mov ecx, dword[ebx+4] 
	mov al, byte[ecx] 

	LOOP:cmp al, byte 0 
	je END
		cmp al, 'A'
		jb NOT_UC
		cmp al, 'Z' 
		ja NOT_UC
		
		sub al, 'A' 
		add al, 'a' 

		NOT_UC:
		call print_char 

		add ecx, 1 
		mov eax, ' ' 
		call print_char 

		mov al, byte[ecx] 
	jmp LOOP
	END: 	
       call print_nl  	
	
	popa
	leave
	ret 
