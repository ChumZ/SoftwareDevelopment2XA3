%include "asm_io.inc"
global asm_main

section .data

section .bss

section .text

asm_main:
	enter 0,0 
	pusha

	mov eax, dword[ebp+12] 
	mov ebx, dword[eax+4] 

	mov al, byte[ebx]
	mov ecx, 0 

	LOOP:cmp al, byte 0 
	je END
		add ecx,1
		add ebx,1
		mov al, byte[ebx] 
	jmp LOOP
	END: 	 


	mov ebx, dword[eax+4] 
	mov al, byte[ebx] 
	
	LOOP2: cmp al, [ecx-1]  
	je THEN 
	  call print_char
	  add ebx, 1
	  mov al, byte[ebx] 

	  THEN:
	    cmp al, 'A' 
	    jb NOT
		
	    cmp al, 'Z' 
	    ja NOT

	    call print_char
	    jmp END2
	  NOT: 
	    add al, 'A'
	    sub al, 'a'	
	    call print_char	 	
	    jmp END2
		
        jmp LOOP2
	END2: 
	call print_nl

	popa
	leave 
	ret 
