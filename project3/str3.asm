%include "asm_io.inc"
global asm_main

section .data
str: dd "second string too long", 0 

section .bss

section .text

asm_main: 
	enter 0,0
	pusha
	
	mov ebx, dword[ebp+12] 
	mov eax, dword[ebx+4] 
	call print_string 
	call print_nl  

	mov ecx, dword[ebx+8]
	mov al, byte[ecx]
	mov ebx, 0  
 
	LOOP:cmp al, byte 0 
	je END  	
		call print_char
		add ecx, 1
		add ebx, 1
		mov al,  byte[ecx]

	jmp LOOP
	END: 
	call print_nl 
 	
	cmp ebx, dword 20 
	  jg THEN
	  jbe PASS 
	THEN: 
	  mov eax, str 
	  call print_string
	  call print_nl
	PASS: 		 	
	   mov eax, ebx 
	   call print_int
	popa
	leave 
	ret 
