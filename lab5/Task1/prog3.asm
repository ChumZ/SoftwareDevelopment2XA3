extern printf

SECTION .data
a: dd 5
fmt: db "%d*%d = %d",10,0

SECTION .text

global main 
main: 

	push ebp
	mov ebp, esp 

	mov eax, [a] 
	mul eax
	push eax 
	mov eax, [a] 
	push eax
	push eax
	push dword fmt
	call printf
	add esp, 12

	mov esp, ebp 
	pop ebp 

	mov eax,0
	ret
	
	

