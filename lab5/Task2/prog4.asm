extern printf 

SECTION .data
x: dd 2
fmt: db "%d*%d +3*%d - 5 = %d", 10, 0 

SECTION .text

global main 
main: 

	push ebp 
	mov ebp, esp 

	mov eax, [x] 
	mul eax 
	mov ebx, eax
	mov eax , 3 
  	mul dword [x] 
	mov ecx, eax 
	mov edx, -5
	mov eax, ebx 
	add eax, ecx 
	add eax, edx
	push eax
	mov eax, [x] 
	push eax
	push eax
	push eax 
	push dword fmt
	call printf
	add esp,16 

	mov esp,ebp
	pop ebp

	mov eax,0
	ret
