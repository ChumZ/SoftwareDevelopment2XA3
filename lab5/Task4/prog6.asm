extern printf 

SECTION .data
	s1: db "hello", 0 
	s2: db "bye", 0
	fmt: db "%s%s", 10, 0 
	fmt1: db "%s", 10, 0 
	
SECTION .text
	global main  

	main: 
		enter 0,0 
		pusha 

		push s1
		push fmt1
		call printf
		add esp, 8

		mov eax, s1
		mov ebx, 0
		mov bl, byte [eax] 
		sub ebx, 'a'
		add ebx, 'A' 
		mov [eax], bl 

		push s1
		push fmt1 
		call printf
		add esp, 8 

	popa
	leave
	ret
