SECTION .data
	fmt1: db "%s",10,0 

SECTION .text
	global main 
	extern printf
	extern strlen

	main:
		enter 0,0 
		pusha

	mov  eax, dword [ebp+12] 
	add eax , 4 
	mov ebx, dword [eax] 
	push ebx
	push fmt1 
	call printf
	add esp, 8  

	mov eax, dword [ebp+12] 
	add eax, 8 
	mov ebx, dword [eax] 
	push ebx 
	push fmt1
	call printf
	add esp ,8 

	popa
	leave 
	ret
